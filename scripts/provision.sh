#!/bin/sh

export DEBIAN_FRONTEND=noninteractive
debconf-set-selections /vagrant/settings/debconf.txt

cp /vagrant/settings/sources.list /etc/apt/

# APTで基本形のLAMP構成
apt-get update
aptitude install -y apache2 git php5 php5-gd php5-json php5-mysql php5-xmlrpc php5-mcrypt php5-xdebug php5-dev php5-sqlite php5-curl php-pear mysql-client mysql-server phpmyadmin optipng vim ruby

rm -rf /var/www/html
ln -fs /vagrant/www /var/www/html

# 各種設定ファイルをコピー
cp /vagrant/settings/000-default.conf /etc/apache2/sites-available
cp /vagrant/settings/php.ini /etc/php5/apache2/php.ini
cp /vagrant/settings/xdebug.ini /etc/php5/mods-available/
cp /vagrant/settings/my.cnf /etc/mysql/

php5enmod mcrypt
a2enmod rewrite headers mime autoindex deflate setenvif expires

# sassインストール
gem install sass

# nodeインストール

curl -sL https://deb.nodesource.com/setup | bash -
apt-get install -y nodejs

npm cache clean -f
npm install -g n
n stable

npm install -g npm@latest

# timezone
echo "Asia/Tokyo" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

sudo service mysql restart
service apache2 restart

