# Vagrantでお手軽LAMP環境を作る

## これは何

LAMP開発環境をローカルのPCに手軽に構築するVagrantファイルです。

以下の環境が、コマンド一発（と待ち時間小一時間）で揃います。

* Ubuntu 14.04LTS
* PHPのありがちな構成
* MySQLのありがちな構成
* phpMyAdmin

これを利用することで、全てのメンバーが（ほぼ）同一構成の仮想マシンで開発を行うことができるようになるんじゃないかなあと思います。

## 依存関係

必須です。

* [VirtualBox](https://www.virtualbox.org/)
* [Vagrant](http://www.vagrantup.com/)

インストーラをダウンロードしてダブルクリックでいいはずなので詳しい説明は省略します。

### 推奨

あると便利

* [Vagrant Cloud](https://vagrantcloud.com/)のアカウント。

## 使い方。

### 仮想サーバー起動

1. zipをダウンロードするなり、フォークしてクローンするなりして、ローカルに保存。
2. 展開したディレクトリに移動して、
```
$ vagrant up
```

Windows用のバッチファイル``vagrant-up.bat``をエクスプローラー上でダブルクリックしてもいいです。

Mac用に``vagrant-up.command``も用意しましたが、適当にぐぐって出たサイトのとおりに書いただけなんで、本当にダブルクリックで動くかどうかは不明。

初回起動時はちょっと時間がかかります（小一時間位）

### ブラウザで確認する

* ``http://localhost:8080/``で、``phpinfo()``の画面が表示されたら無事起動できてます。

### SSHでサーバーにログインする

* ``$ vagrant ssh``でvagrantユーザーとしてログインできます。
* もしくは、
```
$ ssh -p 2222 vagrant@localhost
# パスワードはvagrant
```

### 仮想マシンを止める

* ``$ vagrant halt``

### 仮想マシンを削除する

* ``$ vagrant destroy``

## テスト用のアプリケーションやコンテンツを配置するには

ホストマシン上のwwwディレクトリが仮想サーバーの公開ディレクトリ（DocumentRoot）と同期しています。ここにファイルを配置すればOK。

## MySQL関連

* rootパスワードはpasswordです。
* phpMyAdminのURLは``http://localhost:8080/phpmyadmin``になります。

## 技術的なこと

* 仮想サーバーのプロビジョニングを行っているのはシェルスクリプトです。ChefとかPuppetとかを使えばかっこいいんでしょうが、そのへんは今後の課題ということで。
* プロジェクトごとにカスタマイズしたい場合は、Vagrantfileを書き換えてください。詳細はググってください…

## vagrant shareでURLを共有する

[Vagrant Cloud](https://vagrantcloud.com/)にアカウントを作っておくと、開発環境をインターネット上に公開することができます。

1. Vagrantfileのあるディレクトリに移動する
1. ``$ vagrant login``でVagrantCloudに登録したメールアドレスとパスワードを入力
1. ``$ vagrant share``

以上の手順で、こんな感じの表示になります。
表示されたURLは、``vagrant share``するたびに発行されるものでこのURLで一時的にインターネット上に開発環境を公開することができます。ちょっとした確認には十分使えます。

```
==> default: Your Vagrant Share is running! Name: heavy-peccary-9645
==> default: URL: http://heavy-peccary-9645.vagrantshare.com
```

共有状態を解除するのは、``Ctrl-c``です。

## TODO

* ChefかPuppetを導入する
