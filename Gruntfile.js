'use strict';

/**
 * grunt初期化
 *
 * @param {Object} grunt object of grunt.
 */
module.exports = function(grunt) {
  // load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    // sass
    sass: {
      options: {
        trace: true
      },
      development: {
        options: {
          style: 'expanded'
        },
        files: [{
          expand: true,
          cwd: 'materials/sass',
          src: ['*.scss'],
          dest: 'materials/app/css',
          ext: '.css'
        }]
      },
      production: {
        options: {
          style: 'compressed'
        },
        files: [{
          expand: true,
          cwd: 'materials/sass',
          src: ['*.scss'],
          dest: 'app/public/css',
          ext: '.css'
        }]
      }
    },

    // Jshint
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: [
        'Gruntfile.js',
        'materials/app/js/**/*.js',
        '!app/js/plugins.js',
        '!materials/app/js/vendor/*.js'
      ]
    },

    // htmlhint
    htmlhint: {
      html: {
        options: {
          'tag-pair': true,
          htmlhintrc: '.htmlhintrc'
        },
        src: ['materials/app/**/*.html', '!app/bower_components/**/*.html']
      }
    },

    // autoprefixer関連
    autoprefixer: {
      options: {
        browsers: ['last 2 version', 'ie 8']
      },
      production: {
        files: [{
          expand: true,
          cwd: 'app/public/',
          src: '**/*.css',
          dest: 'app/public/'
        }]
      }
    },

    // ロスレス圧縮関係
    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: 'materials/app',
          src: '**/*.{gif,jpeg,jpg,png,svg}',
          dest: 'app/public'
        }
      ]
    }
  },

  // watch関係
  watch: {
    sass: {
      files: ['materials/**/*.scss'],
      tasks: ['sass'],
      options: {
        livereload: true
      }
    },
    js: {
      files: ['materials/**/*.js', '!Gruntfile.js'],
      tasks: ['jshint', 'uglify:js'],
      options: {
        livereload: true
      }
    },
    js_template: {
      files: ['app/templates/**/*.js', '!app/**/*.min.js'],
      tasks: ['jshint', 'uglify:template']
    },
    htmlhint: {
      files: ['materials/app/**/*.html'],
      tasks: ['htmlhint']
    },
    gruntfile: {
      files: ['Gruntfile.js'],
      tasks: ['jshint']
    },
    img: {
      files: ['materials/app/img/**/*.{gif,jpeg,jpg,png,svg,webp}', 'materials/skins/**/*.{gif,jpeg,jpg,png,svg,webp}'],
      tasks: ['imagemin'],
      options: {
        livereload: true
      }
    },
    bowerFiles: {
      files: ['materials/app/bower_components/**/*'],
      tasks: ['copy'],
      options: {
        livereload: true
      }
    },
    livereload: {
      options: {
        livereload: '<%= connect.options.livereload %>'
      },
      files: [
        'materials/app/{,*/}*.html',
        'materials/app/{,*/}*.css',
        'materials/app/{,*/}*.{gif,jpeg,jpg,png,svg,webp}',
        'materials/app/**/*.js'
      ]
    }
  },

  // connect関係
  // 複数のプロジェクトを同時にwatch or livereloadしたい場合は
  // ポート番号を調整してください
  connect: {
    options: {
      port: 9000,
      livereload: 35729,
      // Change this to '0.0.0.0' to access the server from outside
      hostname: 'localhost'
    },
    livereload: {
      options: {
        open: true,
        base: [
          'materials/app'
        ]
      }
    }
  },

  // minify関連
  // ターゲットはproject_root/app/public/js
  uglify: {
    options: {
      sourceMap: true
    },
    js: {
      files: [{
        expand: true,
        cwd: 'materials/app/js',
        src: '*.js',
        dest: 'app/public/js'
      }]
    },
    template: {
      files: [{
        expand: true,
        cwd: 'app/templates/',
        src: '**/*.js',
        ext: '.min.js',
        dest: 'app/templates'
      }]
    }
  },

  // project_root/app環境にファイルを展開
  copy: {
    js: {
      files: [{
        expand: true,
        dot: true,
        cwd: 'materials/app/js/',
        src: ['vendor/**/*.js'],
        dest: 'app/public/js'
      }]
    }
  }
});

grunt.registerTask('serve', function() {
  grunt.task.run([
    'sass',
    'jshint',
    'uglify',
    'copy',
    'htmlhint',
    'imagemin',
    'connect:livereload',
    'watch'
  ]);
});

grunt.registerTask(
  'dev',
  [
    'sass',
    'jshint',
    'uglify',
    'copy',
    'htmlhint',
    'imagemin'
  ]
);

grunt.registerTask(
  'default',
  [
    'sass',
    'autoprefixer',
    'jshint',
    'uglify',
    'copy',
    'htmlhint',
    'imagemin'
  ]
);

};
